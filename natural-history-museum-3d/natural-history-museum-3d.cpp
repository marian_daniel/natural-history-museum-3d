﻿#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm/gtc/matrix_transform.hpp"
#include "FreeImage.h"
#include <vector>
#include "mesh.h"
#include "fpsController.h"
#include "transform3d.h"
#include "material.h"
#include "texture.h"
#include <iostream>


// Store the current dimensions of the viewport.
glm::vec2 viewportDimensions = glm::vec2(1920, 1080);
glm::vec2 mousePosition = glm::vec2();


// Window resize callback
void resizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    viewportDimensions = glm::vec2(width, height);
}

// This will get called when the mouse moves.
void mouseMoveCallback(GLFWwindow* window, GLdouble mouseX, GLdouble mouseY)
{
    mousePosition = glm::vec2(mouseX, mouseY);
}


int main(int argc, char** argv)
{
    // Initialize GLFW
    glfwInit();

#pragma region Initialise Window

    // Initialize window
    GLFWwindow* window = glfwCreateWindow(viewportDimensions.x, viewportDimensions.y, "Natural History Museum", nullptr, nullptr);

    if (nullptr == window)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();

        return EXIT_FAILURE;
    }

    glfwMakeContextCurrent(window);

    glViewport(0, 0, viewportDimensions.x, viewportDimensions.y);

    // Set window callbacks
    glfwSetFramebufferSizeCallback(window, resizeCallback);
    glfwSetCursorPosCallback(window, mouseMoveCallback);

#pragma endregion

    // Initialize glew
    glewInit();

#pragma region Ininitialise object

    // Instead of coding our vertices, we just load them in from this file in the mesh constructor!
    Mesh* floorModel = new Mesh("./res/models/VRGallery.fbx");
    Mesh* exponate1Model = new Mesh("./res/models/model1.dae");
    Mesh* exponateCearaModel = new Mesh("./res/models/Cearadactylus_v2.fbx");
    Mesh* exponateGalModel = new Mesh("./res/models/Gallimimus.fbx");
    Mesh* standModel = new Mesh("./res/models/stand.fbx");
    Mesh* exponateIchthyModel = new Mesh("./res/models/ichthyosaurus.fbx");
    Mesh* plant1Model = new Mesh("./res/models/plant1.dae");
    Mesh* glassBox1Model = new Mesh("./res/models/GlassBox.dae");
    Mesh* shellsFossilModel = new Mesh("./res/models/shellsFossil.fbx");
    Mesh* coralFossilsModel = new Mesh("./res/models/shell.fbx");
    Mesh* kangarooModel = new Mesh("./res/models/kangaroo.obj");
    Mesh* grassModel = new Mesh("./res/models/grass.obj");
    Mesh* exponateStegoModel = new Mesh("./res/models/Stegosaurus.fbx");
    Mesh* triceratopModel = new Mesh("./res/models/Triceratop.fbx");
    Mesh* crystalModel = new Mesh("./res/models/Krystal.fbx");
    Mesh* tRexHeadModel = new Mesh("./res/models/TRex_Head.fbx");
    Mesh* bird1Model = new Mesh("./res/models/bird1.obj");
    Mesh* lizard1Model = new Mesh("./res/models/lizard1.fbx");
    Mesh* tree1Model = new Mesh("./res/models/tree1.obj");
    Mesh* aquaDinosaur = new Mesh("./res/models/Prefab_Drone_Ophiacodo.fbx");
    Mesh* dinoFossilModel = new Mesh("./res/models/dinoFossil.fbx");
    Mesh* brontosaurModel = new Mesh("./res/models/brontosaurus.fbx");
    Mesh* deerModel = new Mesh("./res/models/deer.dae");

    // The transform being used to draw our second shape.
    Transform3D buildingTransform;
    Transform3D exponate1Transform;
    Transform3D exponateCearaTransform;
    Transform3D exponateGalTransform;
    Transform3D standTransform;
    Transform3D exponateIchthyTransform;
    Transform3D shellsFossilTransform;
    Transform3D coralFossilTransform;
    Transform3D plant1Transform;
    Transform3D glassBox1Transform;
    Transform3D kangarooTransform;
    Transform3D grass1Transform;
    Transform3D exponateStegoTransform;
    Transform3D triceratopTransform;
    Transform3D crystalTransform;
    Transform3D tRexHeadTransform;
    Transform3D bird1Transform;
    Transform3D lizard1Transform;
    Transform3D tree1Transform;
    Transform3D aquaDinosaurTransform;
    Transform3D dinoFossilTransform;
    Transform3D brontosaurTransform;
    Transform3D deerTransform;

    // Make a first person controller for the camera.
    FPSController controller = FPSController();



    // Create Shaders
    Shader* vertexShader = new Shader("./res/shaders/modelLoading.vs", GL_VERTEX_SHADER);
    Shader* fragmentShader = new Shader("./res/shaders/modelLoading.frag", GL_FRAGMENT_SHADER);



    // fields that are used in the shader, on the graphics card
    char cameraViewVS[] = "cameraView";
    char worldMatrixVS[] = "worldMatrix";
    char textureFS[] = "tex";



    // files that we want to open
    char textureFile1[] = "./res/models/textures/galleryTexture.png";
    char textureFile2[] = "./res/models/textures/model1Texture.jpg";
    char textureFileCeara[] = "./res/models/textures/Cearadactylus_diff.jpg";
    char textureFileGal[] = "./res/models/textures/Dino_Skin_brown.jpg";
    char textureFileStand[] = "./res/models/textures/TexturesStand.jpg";
    char textureFileIchthy[] = "./res/models/textures/Ichthyosaurus_diffuse.jpg";
    char textureShellsFossil[] = "./res/models/textures/albedo.png";
    char textureCoralFossil[] = "./res/models/textures/albedo.png";
    char textureGlassBox1[] = "./res/models/textures/glassBoxTexture.png";
    char textureKangaroo[] = "./res/models/textures/kangarooTexture.jpg";
    char textureGrass[] = "./res/models/textures/grass.jpeg";
    char textureFileStego[] = "./res/models/textures/green_skin.jpg";
    char texturePlant1[] = "./res/models/textures/color_plant.png";
    char textureTriceratop[] = "./res/models/textures/OrangeTexture.jpg";
    char textureCrystal[] = "./res/models/textures/lilaTexture.jpg";
    char textureTRexHead[] = "./res/models/textures/TRex_texture.jpg";
    char textureBird1[] = "./res/models/textures/bird1.jpg";
    char textureLizard1[] = "./res/models/textures/lizard.png";
    char textureTree1[] = "./res/models/textures/tree1.jpg";
    char textreAquaDinosaur[] = "./res/models/textures/Img_Ophiacodo_Diff.png";
    char textureDinoFossil[] = "./res/models/textures/dinoFossil.jpg";
    char textureBrontosaur[] = "./res/models/textures/brontosaurus_material.png";
    char textureDeer[] = "./res/models/textures/deer.jpg";

    // Create A Shader Program
    // The class wraps all of the functionality of a gl shader program.
    ShaderProgram* shaderProgram = new ShaderProgram();
    shaderProgram->AttachShader(vertexShader);
    shaderProgram->AttachShader(fragmentShader);



    // Create a material using a texture for our model
    Material* buildingMaterial = new Material(shaderProgram);
    buildingMaterial->SetTexture(textureFS, new Texture(textureFile1));

    Material* exponate1Material = new Material(shaderProgram);
    exponate1Material->SetTexture(textureFS, new Texture(textureFile2));

    Material* exponateCearaMaterial = new Material(shaderProgram);
    exponateCearaMaterial->SetTexture(textureFS, new Texture(textureFileCeara));

    Material* exponateGalMaterial = new Material(shaderProgram);
    exponateGalMaterial->SetTexture(textureFS, new Texture(textureFileGal));

    Material* standMaterial = new Material(shaderProgram);
    standMaterial->SetTexture(textureFS, new Texture(textureFileStand));

    Material* exponateIchthyMaterial = new Material(shaderProgram);
    exponateIchthyMaterial->SetTexture(textureFS, new Texture(textureFileIchthy));

    Material* shellsFossilMaterial = new Material(shaderProgram);
    shellsFossilMaterial->SetTexture(textureFS, new Texture(textureShellsFossil));

    Material* coralFossilMaterial = new Material(shaderProgram);
    coralFossilMaterial->SetTexture(textureFS, new Texture(textureCoralFossil));

    Material* plant1Material = new Material(shaderProgram);
    plant1Material->SetTexture(textureFS, new Texture(texturePlant1));

    Material* glassBox1Material = new Material(shaderProgram);
    glassBox1Material->SetTexture(textureFS, new Texture(textureGlassBox1));

    Material* kangarooMaterial = new Material(shaderProgram);
    kangarooMaterial->SetTexture(textureFS, new Texture(textureKangaroo));

    Material* grass1Material = new Material(shaderProgram);
    grass1Material->SetTexture(textureFS, new Texture(textureGrass));

    Material* exponateStegoMaterial = new Material(shaderProgram);
    exponateStegoMaterial->SetTexture(textureFS, new Texture(textureFileStego));

    Material* triceratopMaterial = new Material(shaderProgram);
    triceratopMaterial->SetTexture(textureFS, new Texture(textureTriceratop));

    Material* crystalMaterial = new Material(shaderProgram);
    crystalMaterial->SetTexture(textureFS, new Texture(textureCrystal));

    Material* tRexHeadMaterial = new Material(shaderProgram);
    tRexHeadMaterial->SetTexture(textureFS, new Texture(textureTRexHead));

    Material* bird1Material = new Material(shaderProgram);
    bird1Material->SetTexture(textureFS, new Texture(textureBird1));

    Material* lizard1Material = new Material(shaderProgram);
    lizard1Material->SetTexture(textureFS, new Texture(textureLizard1));

    Material* tree1Material = new Material(shaderProgram);
    tree1Material->SetTexture(textureFS, new Texture(textureTree1));

    Material* aquaDinosaurMaterial = new Material(shaderProgram);
    aquaDinosaurMaterial->SetTexture(textureFS, new Texture(textreAquaDinosaur));

    Material* dinoFossilMaterial = new Material(shaderProgram);
    dinoFossilMaterial->SetTexture(textureFS, new Texture(textureDinoFossil));

    Material* brontosaurMaterial = new Material(shaderProgram);
    brontosaurMaterial->SetTexture(textureFS, new Texture(textureBrontosaur));

    Material* deerMaterial = new Material(shaderProgram);
    deerMaterial->SetTexture(textureFS, new Texture(textureDeer));
#pragma endregion



    // Main Loop
    while (!glfwWindowShouldClose(window))
    {
        // Exit when escape is pressed.
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) break;

        // Calculate delta time.
        float dt = glfwGetTime();
        // Reset the timer.
        glfwSetTime(0);


        // Update the player controller
        controller.Update(window, viewportDimensions, mousePosition, dt);

        // View matrix.
        glm::mat4 view = controller.GetTransform().GetInverseMatrix();
        // Projection matrix.
        glm::mat4 projection = glm::perspective(.75f, viewportDimensions.x / viewportDimensions.y, .1f, 100.f);
        // Compose view and projection.
        glm::mat4 viewProjection = projection * view;


        // Clear the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.0, 0.0, 0.0, 0.0);


        // Set the camera and world matrices to the shader
        // The string names correspond directly to the uniform names within the shader.
        buildingTransform.SetScale(0.01);
        buildingTransform.SetPosition(glm::vec3(0, 0, 0));
        buildingMaterial->SetMatrix(cameraViewVS, viewProjection);
        buildingMaterial->SetMatrix(worldMatrixVS, buildingTransform.GetMatrix());

        // Bind the material
        buildingMaterial->Bind();
        floorModel->Draw();



        exponate1Transform.SetScale(0.03);
        exponate1Transform.SetPosition(glm::vec3(11, 1, 8));
        exponate1Transform.yMove(1.53);
        exponate1Transform.RotateY(0.01);
        exponate1Material->SetMatrix(cameraViewVS, viewProjection);
        exponate1Material->SetMatrix(worldMatrixVS, exponate1Transform.GetMatrix());

        // Bind the material
        exponate1Material->Bind();
        exponate1Model->Draw();



        exponateCearaTransform.SetScale(0.0004);
        exponateCearaTransform.SetRotation(glm::vec3(4.7, 0, 0));
        exponateCearaTransform.SetPosition(glm::vec3(-25, 1.53, 8));
        exponateCearaTransform.yMove(1.53);
        exponateCearaMaterial->SetMatrix(cameraViewVS, viewProjection);
        exponateCearaMaterial->SetMatrix(worldMatrixVS, exponateCearaTransform.GetMatrix());

        // Bind the material
        exponateCearaMaterial->Bind();
        exponateCearaModel->Draw();



        exponateGalTransform.SetScale(0.006);
        exponateGalTransform.RotateY(0.001);
        exponateGalTransform.SetPosition(glm::vec3(-31, 0.4, 22.4));
        exponateGalMaterial->SetMatrix(cameraViewVS, viewProjection);
        exponateGalMaterial->SetMatrix(worldMatrixVS, exponateGalTransform.GetMatrix());

        // Bind the material
        exponateGalMaterial->Bind();
        exponateGalModel->Draw();



        standTransform.SetScale(0.006);
        //standTransform.RotateY(0.001);
        standTransform.SetPosition(glm::vec3(-31, 0.1, 22.4));
        standMaterial->SetMatrix(cameraViewVS, viewProjection);
        standMaterial->SetMatrix(worldMatrixVS, standTransform.GetMatrix());

        // Bind the material
        standMaterial->Bind();
        standModel->Draw();

        standTransform.SetScale(0.008);
        standTransform.Translate(glm::vec3(30, 0, -14));
        standMaterial->SetMatrix(worldMatrixVS, standTransform.GetMatrix());

        // Bind the material
        standMaterial->Bind();
        standModel->Draw();

        exponateIchthyTransform.SetScale(0.07);
        exponateIchthyTransform.SetRotation(glm::vec3(-1.5, 4.7, 0));
        exponateIchthyTransform.SetPosition(glm::vec3(-25, 1.0, 8));
        exponateIchthyTransform.yMove(1.0);
        exponateIchthyMaterial->SetMatrix(cameraViewVS, viewProjection);
        exponateIchthyMaterial->SetMatrix(worldMatrixVS, exponateIchthyTransform.GetMatrix());

        // Bind the material
        exponateIchthyMaterial->Bind();
        exponateIchthyModel->Draw();



        shellsFossilTransform.SetScale(0.01);
        shellsFossilTransform.SetPosition(glm::vec3(10, 1.5, -9));
        shellsFossilTransform.SetRotation(glm::vec3(0, 4.7, 0));
        shellsFossilMaterial->SetMatrix(cameraViewVS, viewProjection);
        shellsFossilMaterial->SetMatrix(worldMatrixVS, shellsFossilTransform.GetMatrix());

        // Bind the material
        shellsFossilMaterial->Bind();
        shellsFossilModel->Draw();



        coralFossilTransform.SetScale(0.001);
        coralFossilTransform.SetPosition(glm::vec3(0, 5, 0));
        coralFossilTransform.SetRotation(glm::vec3(0, 4.7, 0));
        coralFossilMaterial->SetMatrix(cameraViewVS, viewProjection);
        coralFossilMaterial->SetMatrix(worldMatrixVS, shellsFossilTransform.GetMatrix());

        // Bind the material
        coralFossilMaterial->Bind();
        coralFossilsModel->Draw();



        plant1Transform.SetScale(0.06);
        plant1Transform.SetPosition(glm::vec3(40, 0.2, 20));
        plant1Material->SetMatrix(cameraViewVS, viewProjection);
        plant1Material->SetMatrix(worldMatrixVS, plant1Transform.GetMatrix());

        plant1Material->Bind();
        plant1Model->Draw();



        glassBox1Transform.SetRotation(glm::vec3(0, 1.5, 0));
        glassBox1Transform.SetScale(0.5);
        glassBox1Transform.SetPosition(glm::vec3(0, 0.5, -16.5));
        glassBox1Material->SetMatrix(cameraViewVS, viewProjection);
        glassBox1Material->SetMatrix(worldMatrixVS, glassBox1Transform.GetMatrix());

        glassBox1Material->Bind();
        glassBox1Model->Draw();



        glassBox1Transform.SetRotation(glm::vec3(0, -1.5, 0));
        glassBox1Transform.SetScale(0.5);
        glassBox1Transform.SetPosition(glm::vec3(26, 0.5, 18.5));
        glassBox1Material->SetMatrix(cameraViewVS, viewProjection);
        glassBox1Material->SetMatrix(worldMatrixVS, glassBox1Transform.GetMatrix());

        glassBox1Material->Bind();
        glassBox1Model->Draw();



        kangarooTransform.SetRotation(glm::vec3(-1.5, 0.5, 0));
        kangarooTransform.SetScale(0.018);
        kangarooTransform.SetPosition(glm::vec3(-2.9, 0.2, -19));
        kangarooMaterial->SetMatrix(cameraViewVS, viewProjection);
        kangarooMaterial->SetMatrix(worldMatrixVS, kangarooTransform.GetMatrix());

        kangarooMaterial->Bind();
        kangarooModel->Draw();



        kangarooTransform.SetRotation(glm::vec3(-1.5, 0.9, 0));
        kangarooTransform.SetScale(0.008);
        kangarooTransform.SetPosition(glm::vec3(-1.5, 0.2, -19));
        kangarooMaterial->SetMatrix(cameraViewVS, viewProjection);
        kangarooMaterial->SetMatrix(worldMatrixVS, kangarooTransform.GetMatrix());

        kangarooMaterial->Bind();
        kangarooModel->Draw();



        grass1Transform.SetScale(0.01);
        grass1Transform.SetPosition(glm::vec3(-4.2, 0.2, -19));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(-1.7, 0.2, -19));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(0.8, 0.2, -19));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(3.3, 0.2, -19));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(-4.2, 0.2, -22));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(-1.7, 0.2, -22));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(0.8, 0.2, -22));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(3.3, 0.2, -22));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        //////////


        grass1Transform.SetPosition(glm::vec3(30, 0.5, 20));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        grass1Transform.SetPosition(glm::vec3(27.5, 0.5, 20));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(25, 0.5, 20));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(22.5, 0.5, 20));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(30, 0.5, 23));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(27.5, 0.5, 23));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(25, 0.5, 23));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();


        grass1Transform.SetPosition(glm::vec3(22.5, 0.5, 23));
        grass1Material->SetMatrix(cameraViewVS, viewProjection);
        grass1Material->SetMatrix(worldMatrixVS, grass1Transform.GetMatrix());

        grass1Material->Bind();
        grassModel->Draw();



        triceratopTransform.SetScale(0.0002);
        triceratopTransform.SetRotation(glm::vec3(4.7, 0, 0));
        triceratopTransform.SetPosition(glm::vec3(-33, -0.2, -23));
        triceratopMaterial->SetMatrix(cameraViewVS, viewProjection);
        triceratopMaterial->SetMatrix(worldMatrixVS, triceratopTransform.GetMatrix());

        triceratopMaterial->Bind();
        triceratopModel->Draw();

        tRexHeadTransform.SetScale(0.0004);
        tRexHeadTransform.SetRotation(glm::vec3(4.7, 0, -0.2));
        tRexHeadTransform.SetPosition(glm::vec3(-6, 3, -8));
        tRexHeadMaterial->SetMatrix(cameraViewVS, viewProjection);
        tRexHeadMaterial->SetMatrix(worldMatrixVS, tRexHeadTransform.GetMatrix());

        tRexHeadMaterial->Bind();
        tRexHeadModel->Draw();

        exponateStegoTransform.SetScale(0.0005);
        exponateStegoTransform.SetRotation(glm::vec3(0, 5.3, 0));
        exponateStegoTransform.SetPosition(glm::vec3(-25, -0.1, -8));
        exponateStegoMaterial->SetMatrix(cameraViewVS, viewProjection);
        exponateStegoMaterial->SetMatrix(worldMatrixVS, exponateStegoTransform.GetMatrix());

        // Bind the material
        exponateStegoMaterial->Bind();
        exponateStegoModel->Draw();

        crystalTransform.SetScale(0.002);
        crystalTransform.SetPosition(glm::vec3(28, 4, 8));
        crystalMaterial->SetMatrix(cameraViewVS, viewProjection);
        crystalMaterial->SetMatrix(worldMatrixVS, crystalTransform.GetMatrix());
        // Bind the material
        crystalMaterial->Bind();
        crystalModel->Draw();

        crystalTransform.SetScale(0.001);
        crystalTransform.Translate(glm::vec3(0, 0.5, 1));
        crystalMaterial->SetMatrix(worldMatrixVS, crystalTransform.GetMatrix());
        crystalMaterial->Bind();
        crystalModel->Draw();


        crystalTransform.SetScale(0.0025);
        crystalTransform.Translate(glm::vec3(0, 0.5, -2));
        crystalMaterial->SetMatrix(worldMatrixVS, crystalTransform.GetMatrix());
        crystalMaterial->Bind();
        crystalModel->Draw();

        crystalTransform.SetScale(0.003);
        crystalTransform.Translate(glm::vec3(0, 0.5, 1));
        crystalMaterial->SetMatrix(worldMatrixVS, crystalTransform.GetMatrix());
        crystalMaterial->Bind();
        crystalModel->Draw();



        bird1Transform.SetScale(0.05);
        bird1Transform.SetPosition(glm::vec3(3, 0.6, -19));
        bird1Material->SetMatrix(cameraViewVS, viewProjection);
        bird1Material->SetMatrix(worldMatrixVS, bird1Transform.GetMatrix());

        // Bind the material
        bird1Material->Bind();
        bird1Model->Draw();


        
        lizard1Transform.SetScale(0.003);
        lizard1Transform.SetRotation(glm::vec3(0, -1, 0));
        lizard1Transform.SetPosition(glm::vec3(0, 0.2, -19));
        lizard1Material->SetMatrix(cameraViewVS, viewProjection);
        lizard1Material->SetMatrix(worldMatrixVS, lizard1Transform.GetMatrix());

        // Bind the material
        lizard1Material->Bind();
        lizard1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(-6.5, 0.2, -19));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        // Bind the material
        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(-4.5, 0.2, -19));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        // Bind the material
        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(-2.5, 0.2, -19));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        // Bind the material
        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(-0.5, 0.2, -16));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        // Bind the material
        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(2, 0.2, -17));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        // Bind the material
        tree1Material->Bind();
        tree1Model->Draw();


        aquaDinosaurTransform.SetScale(0.5);
        aquaDinosaurTransform.SetPosition(glm::vec3(-10, 0, 22.4));
        aquaDinosaurTransform.SetRotation(glm::vec3(0, -3, 0));
        aquaDinosaurMaterial->SetMatrix(cameraViewVS, viewProjection);
        aquaDinosaurMaterial->SetMatrix(worldMatrixVS, aquaDinosaurTransform.GetMatrix());
       
        dinoFossilTransform.SetScale(7);
        dinoFossilTransform.RotateY(0.002);
        dinoFossilTransform.SetPosition(glm::vec3(-1, 0.5, 8));
        dinoFossilMaterial->SetMatrix(cameraViewVS, viewProjection);
        dinoFossilMaterial->SetMatrix(worldMatrixVS, dinoFossilTransform.GetMatrix());

        // Bind the material
        dinoFossilMaterial->Bind();
        dinoFossilModel->Draw();

        brontosaurTransform.SetScale(0.018);
        brontosaurTransform.SetRotation(glm::vec3(0, 1.5, 0));
        brontosaurTransform.SetPosition(glm::vec3(-45, 3, 0));
        brontosaurMaterial->SetMatrix(cameraViewVS, viewProjection);
        brontosaurMaterial->SetMatrix(worldMatrixVS, brontosaurTransform.GetMatrix());

        // Bind the material
        brontosaurMaterial->Bind();
        brontosaurModel->Draw();

        brontosaurTransform.SetScale(0.018);
        brontosaurTransform.Translate(glm::vec3(90, 0, -2));
        brontosaurMaterial->SetMatrix(worldMatrixVS, brontosaurTransform.GetMatrix());
        brontosaurMaterial->Bind();
        brontosaurModel->Draw();
       
        aquaDinosaurMaterial->Bind();
        aquaDinosaur->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(28, 0.2, 27));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(26, 0.2, 25));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        tree1Material->Bind();
        tree1Model->Draw();



        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(24, 0.2, 27));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        tree1Material->Bind();
        tree1Model->Draw();



        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(22, 0.2, 25));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        tree1Material->Bind();
        tree1Model->Draw();


        tree1Transform.SetScale(0.018);
        tree1Transform.SetPosition(glm::vec3(20, 0.2, 22));
        tree1Material->SetMatrix(cameraViewVS, viewProjection);
        tree1Material->SetMatrix(worldMatrixVS, tree1Transform.GetMatrix());

        tree1Material->Bind();
        tree1Model->Draw();



        deerTransform.SetScale(2);
        deerTransform.SetRotation(glm::vec3(0, -9, 0));
        deerTransform.SetPosition(glm::vec3(25, 1.7, 22));
        deerMaterial->SetMatrix(cameraViewVS, viewProjection);
        deerMaterial->SetMatrix(worldMatrixVS, deerTransform.GetMatrix());

        deerMaterial->Bind();
        deerModel->Draw();



        buildingMaterial->Unbind();
        exponate1Material->Unbind();
        exponateCearaMaterial->Unbind();
        exponateGalMaterial->Unbind();
        standMaterial->Unbind();
        exponateIchthyMaterial->Unbind();
        plant1Material->Unbind();
        glassBox1Material->Unbind();
        kangarooMaterial->Unbind();
        exponateStegoMaterial->Unbind();
        grass1Material->Unbind();
        triceratopMaterial->Unbind();
        crystalMaterial->Unbind();
        tRexHeadMaterial->Unbind();
        bird1Material->Unbind();
        lizard1Material->Unbind();
        tree1Material->Unbind();
        dinoFossilMaterial->Unbind();
        brontosaurMaterial->Unbind();
        deerMaterial->Unbind();

        // Swap the backbuffer to the front.
        glfwSwapBuffers(window);

        // Poll input and window events.
        glfwPollEvents();
    }

    // Free material should free all objects used by material
    delete buildingMaterial;
    delete exponate1Material;
    delete exponateCearaMaterial;
    delete exponateGalMaterial;
    delete standMaterial;
    delete exponateIchthyMaterial;
    delete plant1Material;
    delete glassBox1Material;
    delete kangarooMaterial;
    delete exponateStegoMaterial;
    delete grass1Material;
    delete triceratopMaterial;
    delete crystalMaterial;
    delete tRexHeadModel;
    delete bird1Material;
    delete lizard1Material;
    delete tree1Material;
    delete dinoFossilMaterial;
    delete brontosaurMaterial;
    delete deerMaterial;

    // Free GLFW memory.
    glfwTerminate();

    // End of Program.
    return 0;
}
